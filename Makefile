antlr-4.9.2-complete.jar:
	wget https://www.antlr.org/download/antlr-4.9.2-complete.jar

agbpycc/antlr/ASMLexer.py agbpycc/antlr/ASMParser.py agbpycc/antlr/ASMVisitor.py &: agbpycc/ASM.g4 antlr-4.9.2-complete.jar
	java -jar antlr-4.9.2-complete.jar $< -visitor -Dlanguage=Python3 -o agbpycc/antlr -Xexact-output-dir

.PHONY: antlr
antlr: agbpycc/antlr/ASMLexer.py agbpycc/antlr/ASMParser.py agbpycc/antlr/ASMVisitor.py

.PHONY: install
install: antlr
	pip install .

.PHONY: setup setup_build
setup: antlr
	pip install -r requirements.txt
setup_build:
	pip install wheel

.PHONY: sdist wheel package
sdist: antlr setup_build
	python setup.py sdist
wheel: antlr setup_build
	python setup.py bdist_wheel
package: sdist wheel
